# Constructor

Go does not have constructors, but we can define functions that act as constructors

```go
func NewFileTransactionLogger(filename string) (TransactionLogger, error) {
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0755)
	if err != nil {
		return nil, fmt.Errorf("cannot open transaction log file: %w", err)
	}

	return &FileTransactionLogger{file: file}, nil
}
```

Note how the function signature returns a `TransactionLogger`, but the function body returns a pointer`$FileTransactionLogger`.

How should we run this?

Have GoRoutines which get passed events via channels.

