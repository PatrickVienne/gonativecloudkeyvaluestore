# Handle Transaction Logs

To read and write transaction logs, the asynchrnous behaviour will be contained within a Run Method, and a ReadEvents Method

```go
// method with defined receiver
func (l *FileTransactionLogger) Run() {
	// buffered Channel allows us to handle bursts without blocking,
	// but to block and focus on processing when the channel gets too full.
	events := make(chan Event, 16)
	l.events = events

	errors := make(chan error, 1)
	l.errors = errors

	go func() {
		for e := range events {
			l.lastSequence++

			// this will not correctly handle entries with whitespace or multiple lines
			// use the csv writer for that.
			_, err := fmt.Fprintf(
				l.file,
				"%d\t%d\t%s\t%s\n",
				l.lastSequence, e.EventType, e.Key, e.Value,
			)

			if err != nil {
				errors <- err
				return
			}
		}
	}()
}

func (l *FileTransactionLogger) ReadEvents() (<-chan Event, <-chan error) {

	scanner := bufio.NewScanner(l.file)
	outEvent := make(chan Event)
	outError := make(chan error, 1)

	go func() {
		var e Event
		defer close(outEvent)
		defer close(outError)

		for scanner.Scan() {
			line := scanner.Text()
            // scan using varbs such as %d, %s
			if _, err := fmt.Sscanf(line, "%d\t%d\t%s\t%s", &e.Sequence, &e.EventType, &e.Key, &e.Value); err != nil {
				outError <- fmt.Errorf("input parse error: %w", err)
				return
			}

			if l.lastSequence >= e.Sequence {
				outError <- fmt.Errorf("transaction numbers out of sequence.")
				return
			}

			l.lastSequence = e.Sequence
		}
		if err := scanner.Err(); err != nil {
			outError <- fmt.Errorf("transaction log read failure: %w", err)
			return
		}

	}()

	return outEvent, outError
}

```