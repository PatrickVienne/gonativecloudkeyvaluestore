package service

import (
	"errors"
	"net/http"
	"sync"
)

var ErrorNoSuchKey = errors.New("no such key")

var Store = struct {
	sync.RWMutex
	M map[string]string
}{M: make(map[string]string)}

func Put(key string, value string) error {
	Store.Lock()
	logger.WritePut(key, value)
	Store.M[key] = value
	Store.Unlock()
	return nil
}

func Delete(key string) error {
	Store.Lock()
	logger.WriteDelete(key)
	delete(Store.M, key)
	Store.Unlock()
	return nil
}

func checkError(err error, w http.ResponseWriter) {
	if errors.Is(err, ErrorNoSuchKey) {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}
