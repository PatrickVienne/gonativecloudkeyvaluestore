# Go Native Key Value Store

## Chapter 1

Start a http Server

```go
func helloGoHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello net/http!\n"))
}

func main() {
	http.HandleFunc("/", helloGoHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
```

Handler is anything which fulfills the interface
```go
type Handler interface {
    ServeHTTP(ResponseWriter, *Request)
}
```

`mux` stands for multiplexer.
The mux makes sure that incoming requests to a specific url are matched to the handling function defined in the application.


