# Go Native Key Value Store

## Chapter 1 - Mux Routing

### Path Variables

```go
r := mux.NewRouter()
r.HandleFunc("/movies/{key}", MoviesHandler)
r.HandleFunc("/movies/{genre}/", GenreCategoriesHandler)
r.HandleFunc("/movies/{genre}/{id:[0-9]+}", GenreHandler)
```

Path Variables can be requested from `mux` via

```go
vars := mux.Vars(req)
genre := vars["genre"]
```

### URL Request Constraints (Matchers)


```go
r := mux.NewRouter()
r.HandleFunc("/movies/{key}", MoviesHandler)
	.Host("www.yourdomainhere.com")
	.Methods("GET", "HOST")
	.Schemes("http")
```
