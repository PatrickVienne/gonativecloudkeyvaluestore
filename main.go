package main

import (
	"io"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/PatrickVienne/gonativecloudkeyvaluestore/service"
)

func helloGoHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello net/http!\n"))
}

func keyValueGetHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["key"]

	service.Store.RLock()
	value, ok := service.Store.M[key]
	service.Store.RUnlock()

	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	w.Write([]byte(value))
}

func keyValuePutHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["key"]

	value, err := io.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = service.Put(key, string(value))

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func main() {
	service.Initialization()

	r := mux.NewRouter()
	r.HandleFunc("/", helloGoHandler)

	// put something to this endpoint with:
	// curl -X PUT -d 'HelloGoNative' localhost:8080/v1/key/key-1
	r.HandleFunc("/v1/key/{key}", keyValuePutHandler).Methods("PUT")

	// Retrieve Data
	// $ curl -X GET localhost:8080/v1/key/key-1
	//HelloGoNative
	r.HandleFunc("/v1/key/{key}", keyValueGetHandler).Methods("GET")

	log.Fatal(http.ListenAndServe(":8080", r))
}
