# Go Native Key Value Store

## Chapter 3 - Locks

Creating a lockable map

```go
var myMap = struct {
	sync.Mutex
	m map[string]string
}{m: make(map[string]string)}
```

Then we can make access to our maps concurrency safe for writing
```go
myMap.Lock()
myMap.m["myKey"] = "myValue"
myMap.UnLock()
```

and reading
```go
myMap.RLock()
myValue := myMap.m["myKey"]
myMap.RUnLock()
```

