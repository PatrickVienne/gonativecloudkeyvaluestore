# Stateless Applications

Statelessness can have 2 characteristics, application state, ressource state

Application state is a major issue, due to server affinity. 
A request has to be routed to the same server as the previous one to continue working as expected.

We will track the state with a transaction logger.

Each line has 4 contents

An interface should help to keep the structure flexible, and give us room to use another method should we decide to switch from file logging to db tracking of the state.